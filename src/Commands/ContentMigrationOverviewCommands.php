<?php

namespace Drupal\content_migration_overview\Commands;

use Drupal\content_migration_overview\Form\CredentialForm;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\FileUrlGenerator;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\migrate_drupal\MigrationConfigurationTrait;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * A drush command file.
 */
class ContentMigrationOverviewCommands extends DrushCommands {

  use MigrationConfigurationTrait;
  use StringTranslationTrait;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The file url generator service.
   *
   * @var \Drupal\Core\File\FileUrlGenerator
   */
  protected $fileUrlGenerator;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a new ContentMigrationOverviewCommands object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\File\FileUrlGenerator $fileUrlGenerator
   *   The file url generator service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(Connection $database, StateInterface $state, FileSystemInterface $file_system, FileUrlGenerator $fileUrlGenerator, RendererInterface $renderer) {
    $this->database         = $database;
    $this->state            = $state;
    $this->fileSystem       = $file_system;
    $this->fileUrlGenerator = $fileUrlGenerator;
    $this->renderer         = $renderer;
  }

  /**
   * Drush Command to generate Migration Stat.
   *
   * @command migration:stat
   * @usage drush mstat
   * @aliases mstat
   */
  public function migrationStat() {
    // Check the source database connection and log an error if it's not found.
    $status = $this->checkSourceConnection();

    if (!$status) {
      $this->logger()->error($this->t('No source database connection found.'));
      $this->logger()->error($this->t('Please configure the source database credentials at :config_page', [
        ':config_page' => Url::fromRoute('content_migration_overview.migrate_database_credentials', [], ['absolute' => TRUE])->toString(),
      ]));
      return;
    }

    $prefix_length = 0;
    $database      = $this->state->get('migrate_database');

    if (!empty($database)) {
      $prefix_length = strlen($database['prefix']);
    }

    // Users Stat.
    $d7_users_query = "SELECT uid FROM {users} WHERE uid > 0";
    $d7_users       = $this->getValues($d7_users_query, 'd7', 'one_column');
    $d7_users_count = count($d7_users);

    $d8_users_query = "SELECT sourceid1, destid1 FROM {migrate_map_d7_user}";
    $d8_users       = $this->getValues($d8_users_query, 'd8', 'two_column');

    $users_matches     = array_intersect_key($d7_users, $d8_users);
    $users_passed      = count($users_matches);
    $users_failed      = $d7_users_count - $users_passed;
    $users_failed_data = array_diff_key($d7_users, $d8_users);

    // Node Stat.
    $nodes                   = $nodes_failed_data = [];
    $node_types              = $this->getValues("SELECT type,name FROM {node_type}", 'd7', 'two_column');
    $node_type_mapping_query = "SELECT sourceid1, destid1 FROM {migrate_map_d7_node_type} ORDER BY sourceid1 ASC";
    $node_type_mapping       = $this->getValues($node_type_mapping_query, 'd8', 'two_column');

    foreach ($node_type_mapping as $d7_node_type => $d8_node_type) {
      $type          = $node_types[$d7_node_type] . " (Machine name: " . $d7_node_type . ")";
      $d7_nids_query = "SELECT nid FROM {node} WHERE type = '{$d7_node_type}'";
      $d7_nids       = $this->getValues($d7_nids_query, 'd7', 'one_column');
      $d7_nids_count = count($d7_nids);

      if ($d8_node_type) {
        // Default generated table names, limited to 63 characters.
        $map_table_name = "migrate_map_d7_node_complete__" . $d7_node_type;
        $map_table_name = mb_substr($map_table_name, 0, 63 - $prefix_length);

        $d8_nids_query = "SELECT sourceid1, destid1 FROM {$map_table_name}";
        $d8_nids       = $this->getValues($d8_nids_query, 'd8', 'two_column');

        $nodes_matches = array_intersect_key($d7_nids, $d8_nids);
        $nodes_passed  = count($nodes_matches);
        $nodes_failed  = $d7_nids_count - $nodes_passed;

        $node_type_failed_data = array_diff_key($d7_nids, $d8_nids);
        $nodes_failed_data     = array_merge($nodes_failed_data, $node_type_failed_data);

        $nodes[$node_types[$d7_node_type]] = [
          'type'        => $type,
          'total'       => $d7_nids_count,
          'passed'      => $nodes_passed,
          'failed'      => $nodes_failed,
          'failed_data' => $node_type_failed_data,
        ];
      }
      else {
        $nodes_failed_data                 = array_merge($nodes_failed_data, $d7_nids);
        $nodes[$node_types[$d7_node_type]] = [
          'type'        => $type,
          'total'       => $d7_nids_count,
          'passed'      => 0,
          'failed'      => $d7_nids_count,
          'failed_data' => $d7_nids,
        ];
      }
    }

    $nodes_total  = array_sum(array_column($nodes, 'total'));
    $nodes_passed = array_sum(array_column($nodes, 'passed'));
    $nodes_failed = array_sum(array_column($nodes, 'failed'));

    // Taxonomy Stat.
    $taxonomies                 = $taxonomies_failed_data = [];
    $vocabularies_machine_names = $this->getValues("SELECT vid,machine_name FROM {taxonomy_vocabulary}", 'd7', 'two_column');
    $vocabularies               = $this->getValues("SELECT vid,name FROM {taxonomy_vocabulary}", 'd7', 'two_column');
    $vocabulary_mapping_query   = "SELECT sourceid1, destid1 FROM {migrate_map_d7_taxonomy_vocabulary} ORDER BY destid1 ASC";
    $vocabulary_mapping         = $this->getValues($vocabulary_mapping_query, 'd8', 'two_column');

    foreach ($vocabulary_mapping as $d7_vocabulary => $d8_vocabulary) {
      $type          = $vocabularies[$d7_vocabulary] . " (Machine name: " . $vocabulary_mapping[$d7_vocabulary] . ")";
      $d7_tids_query = "SELECT tid FROM {taxonomy_term_data} WHERE vid = {$d7_vocabulary}";
      $d7_tids       = $this->getValues($d7_tids_query, 'd7', 'one_column');
      $d7_tids_count = count($d7_tids);

      if ($d8_vocabulary) {
        // Default generated table names, limited to 63 characters.
        $map_table_name = "migrate_map_d7_taxonomy_term__" . $vocabularies_machine_names[$d7_vocabulary];
        $map_table_name = mb_substr($map_table_name, 0, 63 - $prefix_length);

        $d8_tids_query = "SELECT sourceid1, destid1 FROM {$map_table_name}";
        $d8_tids       = $this->getValues($d8_tids_query, 'd8', 'two_column');

        $taxonomies_matches = array_intersect_key($d7_tids, $d8_tids);
        $taxonomies_passed  = count($taxonomies_matches);
        $taxonomies_failed  = $d7_tids_count - $taxonomies_passed;

        $vocabulary_failed_data = array_diff_key($d7_tids, $d8_tids);
        $taxonomies_failed_data = array_merge($taxonomies_failed_data, $vocabulary_failed_data);

        $taxonomies[$vocabularies[$d7_vocabulary]] = [
          'type'        => $type,
          'total'       => $d7_tids_count,
          'passed'      => $taxonomies_passed,
          'failed'      => $taxonomies_failed,
          'failed_data' => $vocabulary_failed_data,
        ];
      }
      else {
        $taxonomies_failed_data                    = array_merge($taxonomies_failed_data, $d7_tids);
        $taxonomies[$vocabularies[$d7_vocabulary]] = [
          'type'        => $type,
          'total'       => $d7_tids_count,
          'passed'      => 0,
          'failed'      => $d7_tids_count,
          'failed_data' => $d7_tids,
        ];
      }
    }

    $taxonomies_total  = array_sum(array_column($taxonomies, 'total'));
    $taxonomies_passed = array_sum(array_column($taxonomies, 'passed'));
    $taxonomies_failed = array_sum(array_column($taxonomies, 'failed'));

    // Print the summary table.
    $output = new ConsoleOutput();
    $output->writeln('<info>Migration Summary</info>');
    $summary_header = ['Type', 'Total', 'Passed', 'Failed'];
    $summary_rows   = [
      'User' => [
        'type'   => 'Users',
        'total'  => $d7_users_count,
        'passed' => $users_passed,
        'failed' => $users_failed,
      ],
      'Node' => [
        'type'   => 'Nodes',
        'total'  => $nodes_total,
        'passed' => $nodes_passed,
        'failed' => $nodes_failed,
      ],
      'Term' => [
        'type'   => 'Taxonomies',
        'total'  => $taxonomies_total,
        'passed' => $taxonomies_passed,
        'failed' => $taxonomies_failed,
      ],
    ];
    $this->printTable($summary_header, $summary_rows, $output);

    // Generate the summary row data.
    $summary_rows['User']['pass']        = round(($summary_rows['User']['passed'] / $summary_rows['User']['total']) * 100, 2);
    $summary_rows['User']['fail']        = round(($summary_rows['User']['failed'] / $summary_rows['User']['total']) * 100, 2);
    $summary_rows['User']['failed_data'] = $users_failed_data;

    $summary_rows['Node']['pass']        = round(($summary_rows['Node']['passed'] / $summary_rows['Node']['total']) * 100, 2);
    $summary_rows['Node']['fail']        = round(($summary_rows['Node']['failed'] / $summary_rows['Node']['total']) * 100, 2);
    $summary_rows['Node']['path']        = $this->fileUrlGenerator->generateAbsoluteString('public://migration-reports/node_migration_summary_report.html');
    $summary_rows['Node']['failed_data'] = $nodes_failed_data;

    $summary_rows['Term']['pass']        = round(($summary_rows['Term']['passed'] / $summary_rows['Term']['total']) * 100, 2);
    $summary_rows['Term']['fail']        = round(($summary_rows['Term']['failed'] / $summary_rows['Term']['total']) * 100, 2);
    $summary_rows['Term']['path']        = $this->fileUrlGenerator->generateAbsoluteString('public://migration-reports/taxonomy_migration_summary_report.html');
    $summary_rows['Term']['failed_data'] = $taxonomies_failed_data;

    // Generate HTML report.
    $this->generateHtmlReport($summary_rows, $nodes, $taxonomies);
  }

  /**
   * Prints tabular data.
   */
  public function printTable($header, $rows, $output) {
    // Initialize table.
    $table = new Table($output);

    // Set the table header and add rows to the table.
    $table->setHeaders($header);
    $table->addRows($rows);

    // Render the table.
    $table->render();
  }

  /**
   * Generate HTML report.
   */
  public function generateHtmlReport($summary, $node_summary, $taxonomy_summary) {
    // Migration Summary.
    $summary_markup = [
      '#theme'   => 'migration_summary',
      '#summary' => $summary,
    ];

    $summary_markup = $this->renderer->renderInIsolation($summary_markup);

    // Node Summary.
    $node_summary_markup = [
      '#theme'   => 'node_migration_summary',
      '#summary' => $node_summary,
    ];

    $node_summary_markup = $this->renderer->renderInIsolation($node_summary_markup);

    // Taxonomy Summary.
    $taxonomy_summary_markup = [
      '#theme'   => 'taxonomy_migration_summary',
      '#summary' => $taxonomy_summary,
    ];

    $taxonomy_summary_markup = $this->renderer->renderInIsolation($taxonomy_summary_markup);

    // HTML Report Generation.
    $directory = 'public://migration-reports/';

    // Prepare the directory and ensure it's writable.
    if ($this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY)) {
      // Save the Migration Summary Report file.
      $summary_file_path = $directory . 'migration_summary_report.html';
      $summary_file      = $this->fileSystem->saveData($summary_markup->__toString(), $summary_file_path, FileExists::Replace);

      if ($summary_file) {
        $path = $this->fileUrlGenerator->generateAbsoluteString($summary_file);
        $this->logger()->success(dt('Migration Summary Report generated at @path', ['@path' => $path]));
      }
      else {
        $this->logger()->error(dt('An error occurred while creating the Migration Summary Report HTML file.'));
      }

      // Save the Node Migration Summary Report file.
      $node_summary_file_path = $directory . 'node_migration_summary_report.html';
      $node_summary_file      = $this->fileSystem->saveData($node_summary_markup->__toString(), $node_summary_file_path, FileExists::Replace);

      if ($node_summary_file) {
        $path = $this->fileUrlGenerator->generateAbsoluteString($node_summary_file);
        $this->logger()->success(dt('Node Migration Summary Report generated at @path', ['@path' => $path]));
      }
      else {
        $this->logger()->error(dt('An error occurred while creating the Node Migration Summary Report HTML file.'));
      }

      // Save the Taxonomy Migration Summary Report file.
      $taxonomy_summary_file_path = $directory . 'taxonomy_migration_summary_report.html';
      $taxonomy_summary_file      = $this->fileSystem->saveData($taxonomy_summary_markup->__toString(), $taxonomy_summary_file_path, FileExists::Replace);

      if ($taxonomy_summary_file) {
        $path = $this->fileUrlGenerator->generateAbsoluteString($taxonomy_summary_file);
        $this->logger()->success(dt('Taxonomy Migration Summary Report generated at @path', ['@path' => $path]));
      }
      else {
        $this->logger()->error(dt('An error occurred while creating the Taxonomy Migration Summary Report HTML file.'));
      }
    }
    else {
      $this->logger()->error(dt('The public directory is not writable.'));
    }
  }

  /**
   * Check the source database connection.
   */
  public function checkSourceConnection() {
    $status = FALSE;

    $available_connections = array_diff(array_keys(Database::getAllConnectionInfo()), ['default']);
    $available_connections = array_combine($available_connections, $available_connections);

    if (isset($available_connections['migrate'])) {
      $status = TRUE;
    }
    else {
      $driver   = $this->state->get('migrate_database_driver');
      $database = $this->state->get('migrate_database');

      if (!empty($driver) && !empty($database)) {
        $status = TRUE;
      }
    }

    return $status;
  }

  /**
   * Get the values from the database.
   */
  public function getValues($query, $drupal_version, $result_type) {
    if ($drupal_version == 'd8') {
      $connection = $this->database;
    }

    if ($drupal_version == 'd7') {
      $available_connections = array_diff(array_keys(Database::getAllConnectionInfo()), ['default']);
      $available_connections = array_combine($available_connections, $available_connections);

      if (isset($available_connections['migrate'])) {
        $connection = Database::getConnection('default', 'migrate');
      }
      else {
        $driver   = $this->state->get('migrate_database_driver');
        $database = $this->state->get('migrate_database');

        if (!empty($driver) && !empty($database)) {
          $drivers           = CredentialForm::getDatabaseTypes();
          $reflection        = new \ReflectionClass($drivers[$driver]);
          $install_namespace = $reflection->getNamespaceName();

          // Cut the trailing \Install from namespace.
          $database['namespace'] = substr($install_namespace, 0, strrpos($install_namespace, '\\'));
          $database['driver']    = $driver;

          $connection = $this->getConnection($database);
        }
      }
    }

    if (!empty($connection)) {
      $result = $connection->query($query);

      if ($result_type == 'node') {
        return $result->fetchAllAssoc('nid', \PDO::FETCH_ASSOC);
      }
      elseif ($result_type == 'one_column') {
        return $result->fetchAllKeyed(0, 0);
      }
      elseif ($result_type == 'two_column') {
        return $result->fetchAllKeyed(0, 1);
      }
    }
  }

}
