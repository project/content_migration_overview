<?php

namespace Drupal\content_migration_overview\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\migrate_drupal\MigrationConfigurationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Migrate Upgrade database credential form.
 */
class CredentialForm extends FormBase {

  use MigrationConfigurationTrait;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * An array of error information.
   *
   * @var array
   */
  protected $errors = [];

  /**
   * Constructs a new CredentialForm object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'migration_credentials_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $drivers        = $this->getDatabaseTypes();
    $drivers_keys   = array_keys($drivers);
    $default_driver = current($drivers_keys);

    $form['database'] = [
      '#type'        => 'details',
      '#title'       => $this->t('Source database'),
      '#description' => $this->t('Provide credentials for the database of the source <strong>Drupal 7</strong> site.'),
      '#open'        => TRUE,
    ];

    $form['database']['driver'] = [
      '#type'          => 'radios',
      '#title'         => $this->t('Database type'),
      '#required'      => TRUE,
      '#default_value' => $this->state->get('migrate_database_driver') ?? $default_driver,
    ];

    // If there is only one available database driver, disable the element.
    if (count($drivers) == 1) {
      $form['database']['driver']['#disabled'] = TRUE;
    }

    // Add driver-specific configuration options.
    foreach ($drivers as $key => $driver) {
      $form['database']['driver']['#options'][$key] = $driver->name();
      $form['database']['settings'][$key]           = $driver->getFormOptions([]);
      unset($form['database']['settings'][$key]['advanced_options']['prefix']['#description']);

      // This form is not rebuilt during submission so #limit_validation_errors
      // is not used. The database and username fields for mysql and pgsql must
      // not be required.
      $form['database']['settings'][$key]['database']['#required'] = FALSE;
      $form['database']['settings'][$key]['username']['#required'] = FALSE;
      $form['database']['settings'][$key]['database']['#states']   = [
        'required' => [
          ':input[name=driver]' => ['value' => $key],
        ],
      ];

      if (!str_ends_with($key, '\\sqlite')) {
        $form['database']['settings'][$key]['username']['#states'] = [
          'required' => [
            ':input[name=driver]' => ['value' => $key],
          ],
        ];
        $form['database']['settings'][$key]['password']['#states'] = [
          'required' => [
            ':input[name=driver]' => ['value' => $key],
          ],
        ];
      }

      $form['database']['settings'][$key]['#prefix']                      = '<h2 class="js-hide">' . $this->t('@driver_name settings', ['@driver_name' => $driver->name()]) . '</h2>';
      $form['database']['settings'][$key]['#type']                        = 'container';
      $form['database']['settings'][$key]['#tree']                        = TRUE;
      $form['database']['settings'][$key]['advanced_options']['#parents'] = [$key];
      $form['database']['settings'][$key]['#states']                      = [
        'visible' => [
          ':input[name=driver]' => ['value' => $key],
        ],
      ];

      // Move the host fields out of advanced settings.
      if (isset($form['database']['settings'][$key]['advanced_options']['host'])) {
        $form['database']['settings'][$key]['host']            = $form['database']['settings'][$key]['advanced_options']['host'];
        $form['database']['settings'][$key]['host']['#title']  = 'Database host';
        $form['database']['settings'][$key]['host']['#weight'] = -1;
        unset($form['database']['settings'][$key]['database']['#default_value']);
        unset($form['database']['settings'][$key]['advanced_options']['host']);
      }

      // Set database default values.
      if ($this->state->get('migrate_database')) {
        foreach ($this->state->get('migrate_database') as $k => $v) {
          if (isset($form['database']['settings'][$key][$k])) {
            $form['database']['settings'][$key][$k]['#default_value'] = $v;
          }
        }
      }
    }

    $form['actions']['#type']  = 'actions';
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => $this->t('Save'),
      '#button_type' => 'primary',
      '#weight'      => 10,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the database driver from the form, use reflection to get the
    // namespace, and then construct a valid database array the same as in
    // settings.php.
    $driver            = $form_state->getValue('driver');
    $drivers           = $this->getDatabaseTypes();
    $reflection        = new \ReflectionClass($drivers[$driver]);
    $install_namespace = $reflection->getNamespaceName();

    $database = $form_state->getValue($driver);
    // Cut the trailing \Install from namespace.
    $database['namespace'] = substr($install_namespace, 0, strrpos($install_namespace, '\\'));
    $database['driver']    = $driver;

    // Validate the driver settings and just end here if we have any issues.
    if ($errors = $drivers[$driver]->validateDatabaseSettings($database)) {
      foreach ($errors as $name => $message) {
        $this->errors[$name] = $message;
      }
    }

    // Get the Drupal version of the source database so it can be validated.
    $error_key = $database['driver'] . '][database';
    if (!$this->errors) {
      try {
        $this->getConnection($database);
      }
      catch (\Exception $e) {
        $msg = $this->t('Failed to connect to your database server. The server reports the following message: %error.<ul><li>Is the database server running?</li><li>Does the database exist, and have you entered the correct database name?</li><li>Have you entered the correct username and password?</li><li>Have you entered the correct database hostname?</li></ul>', ['%error' => $e->getMessage()]);
        $this->errors[$error_key] = $msg;
      }
    }

    // Display all errors as a list of items.
    if ($this->errors) {
      $form_state->setError($form, $this->t('<h3>Resolve all issues below to continue.</h3>'));
      foreach ($this->errors as $name => $message) {
        $form_state->setErrorByName($name, $message);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Store information in the state variable.
    $driver = $form_state->getValue('driver');
    $this->state->set('migrate_database_driver', $driver);
    $this->state->set('migrate_database', $form_state->getValue($driver));
  }

  /**
   * Returns all supported database driver installer objects.
   *
   * @return \Drupal\Core\Database\Install\Tasks[]
   *   An array of available database driver installer objects.
   */
  public static function getDatabaseTypes() {
    // Make sure the install API is available.
    include_once DRUPAL_ROOT . '/core/includes/install.inc';
    $database_types = [];
    foreach (Database::getDriverList()->getInstallableList() as $name => $driver) {
      $database_types[$name] = $driver->getInstallTasks();
    }
    return $database_types;
  }

}
