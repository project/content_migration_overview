# Content Migration Overview

The Content Migration Overview module provides a comprehensive summary of your
content migration in Drupal.

## FEATURES

It provides a Drush command to validate the migrated content data.

## REQUIREMENTS

This module requires no modules outside of Drupal core.

## INSTALLATION

### Using the Drupal User Interface (easy):

1. Navigate to the 'Extend' page (admin/modules) via the manage administrative
   menu.
2. Locate the Content Migration Overview module and select the checkbox next to
   it.
3. Click on 'Install' to enable the Content Migration Overview module.

### Or use the command line (advanced, but very efficient).

- To enable Content Migration Overview module with Drush, execute the command
  below: <br> `drush en content_migration_overview`

## CONFIGURATION
Define the source database connection with the key '**migrate**' in addition to
the 'default' database connection in settings.php:

**OR**

Configure the source database connection at
`/admin/config/system/migrate-database-credentials`.

## MAINTAINERS

- Vishal Kadam (vishal.kadam) - https://www.drupal.org/user/3622517
